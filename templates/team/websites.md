{% import "_macros.html" as macro %}

The Websites Team maintains pages like [This Week in GNOME](https://thisweek.gnome.org/), [Apps for GNOME](https://apps.gnome.org/), and many more. You can find an overview of all websites on our [team page](https://gitlab.gnome.org/Teams/Websites/). The websites team is supported by the work of the design and infrastructure teams.

<ul class="links">
  {% call macro::link("gitlab", "https://gitlab.gnome.org/Teams/Websites/", "Websites Team"|t, "Home of the Websites Team on GitLab"|t) %}
  {% call macro::link("docs", "https://brand.gnome.org/", "Brand Book"|t, "Style guide for designing print material and websites for GNOME"|t) %}
  {% call macro::link("chat", "https://matrix.to/#/#pages:gnome.org", "#pages:gnome.org", "Chat for discussing websites"|t) %}
</ul>

## Translating websites

{% let translate_url %}
{% if site.document_language == "en" %}
  {% let translate_url = "https://l10n.gnome.org/releases/gnome-infrastructure/".to_string() %}
{% else %}
    {% let translate_url = ("https://l10n.gnome.org/languages/{0}/gnome-infrastructure/ui/"|f([site.lang.tag_unix.as_str()])).to_string() %}
{% endif %}

<p>{{ "Some of our websites are translated into different languages. You can find the translations in the [*Infrastructure* category]({0})."|t|f([translate_url])|markdown }}</p>

## Reporting issues

Oftentimes a website has a link to its repository at the bottom of the page. If you can’t figure out the correct place to report an issue you can check the [Website Team’s page](https://gitlab.gnome.org/Teams/Websites/) for the respective website. This page also gives you a place to report issues without knowing the correct website or for more general issues.

## Working on websites

The work on websites highly differs from website to website. For getting started it is a good idea to resist the temptation to redesign a whole website or even the whole of all websites. Instead, you can check the [team’s issue tracker](https://gitlab.gnome.org/groups/Teams/Websites/-/issues/) for open issues. You can also check what website suits you technology wise: While some websites are written in Sphinx, Jekyll, or plain HTML, others are based on more complex Rust code.

### Useful apps

<div>
  <ul class="app-grid">
    {% call macro::app("org.gnome.Builder") %}
    {% call macro::app("com.rafaelmardojai.SharePreview") %}
    {% call macro::app("org.gnome.design.Lorem") %}
    {% call macro::app_flathub("Contrast"|t, "org.gnome.design.Contrast") %}
    {% call macro::app_flathub("Palette"|t, "org.gnome.design.Palette") %}
    {% call macro::app_flathub("Typography"|t, "org.gnome.design.Typography") %}
  </ul>
</div>
