use crate::page;
use color_eyre::Help;

use page_tools::{
    i18n, markdown,
    output::{self, Dir, Output, OutputSimple, Scss},
    Apps, AppsMinimal, Language, Site,
};

use std::sync::Arc;

pub fn generate() -> color_eyre::Result<()> {
    output::prepare();

    let apps = Apps::from_upstream_cached().unwrap();

    Dir::from("assets/").output("assets/");

    Scss::from("scss/main.scss").output("assets/style/main.css");

    i18n::init(env!("CARGO_PKG_NAME"));
    let languages = i18n::languages();
    for lang in languages.iter() {
        let lang_tag_ietf = &lang.tag_ietf;

        log::info!("Building pages for '{}'", lang.tag_unix);
        let apps_minimal_result = AppsMinimal::from_upstream_cached(lang.tag_unix.clone());

        let apps_minimal = match apps_minimal_result {
            Ok(apps_minimal) => apps_minimal,
            Err(err) => {
                warn!("No localized app info for {lang_tag_ietf}: {err}");
                AppsMinimal::from_upstream_cached("en".to_string()).unwrap()
            }
        };

        let mut apps = apps.clone();
        // Translate name and description in app
        apps.partial_translate(&apps_minimal);

        pages(Arc::new(apps), lang)
            .with_note(|| format!("Could not create page for {lang_tag_ietf}."))?;

        // Write translated apps API
        serde_json::to_string(&apps_minimal)
            .unwrap()
            .output(format!(
                "assets/data/{lang_tag_ietf}/{}",
                AppsMinimal::FILENAME
            ));
    }

    let languages_js = page_tools::i18n::templates::LanguagesJs::new(languages);
    languages_js.output("assets/js/languages.js");

    output::complete()?;

    Ok(())
}

fn pages(apps: Arc<Apps>, language: &Language) -> color_eyre::Result<()> {
    let lang = &language.tag_unix;
    i18n::set_lang(lang);

    let site = Arc::new(Site::new(language));

    let overview = page::Index {
        site: &site,
        content: markdown::template_to_html(&page::IndexContent { site: &site }, "index.md")?,
    };
    overview.output_localized("index.html", lang);

    let design = page::Design {
        site: &site,
        content: markdown::template_to_html(&(page::DesignContent {}), "team/design.md")?,
    };
    design.output_localized("team/design/index.html", lang);

    /*
    let translation = page::Governance {
        site: &site,
        content: markdown::template_to_html(
            &(page::GovernanceContent { site: &site }),
            "team/design.md",
        )?,
    };
    translation.output_localized("team/governance/index.html", lang);

    let engagement = page::Engagement {
        site: &site,
        content: markdown::template_to_html(
            &(page::EngagementContent { site: &site }),
            "team/engagement.md",
        )?,
    };
    engagement.output_localized("team/engagement/index.html", lang);
     */

    let websites = page::Websites {
        site: &site,
        content: markdown::template_to_html(
            &(page::WebsitesContent {
                site: &site,
                apps: &apps,
            }),
            "team/websites.md",
        )?,
    };
    websites.output_localized("team/websites/index.html", lang);

    let coding = page::Programming {
        site: &site,
        content: markdown::template_to_html(
            &page::ProgrammingContent {
                site: &site,
                apps: &apps,
            },
            "index.md",
        )?,
    };
    coding.output_localized("team/programming/index.html", lang);

    let mut tasks = Vec::new();
    for app in apps.values() {
        debug!("App ({lang}): {}", app.id);

        let apps_ = apps.clone();
        let site_ = site.clone();
        let lang_ = lang.clone();
        let app_ = app.clone();

        let task = async_global_executor::spawn_blocking(move || {
            let app_page = page::App {
                site: &site_,
                app: &app_,
                content: markdown::template_to_html(
                    &page::AppContent {
                        site: &site_,
                        app: &app_,
                        apps: &apps_,
                    },
                    "app.md",
                )
                .unwrap(),
            };
            app_page.output_localized(format!("app/{}/index.html", app_.page_id), &lang_);
        });

        tasks.push(task);
    }

    async_global_executor::block_on(futures::future::join_all(tasks));

    Ok(())
}
