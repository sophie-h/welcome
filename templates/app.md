{% import "_macros.html" as macro %}

{% let ago_url = "https://apps.gnome.org/{}/"|format(app.page_id) %}

<p>{{ "On this page you can learn how to contribute to *{0}*. If you are interested in general information about this app, please visit the [Apps for GNOME page]({1})."|t|f([app.name.as_str(), ago_url.as_str()])|markdown }}</p>

<ul class="links">
  {% call macro::link("docs", ago_url, "Apps for GNOME"|t, "Learn more about this app on apps.gnome.org"|t) %}
</ul>

## Translating the app

The original text in apps is usually written in American English. The translation into other languages can be added by translators separately. The translation usually encompasses the apps’ user interface as well as the metadata shown in places like [Software](https://apps.gnome.org/app/org.gnome.Software/) or [apps.gnome.org](https://apps.gnome.org). Common abbreviations used in this context are *l10n* (localization) and *i18n* (internationalization).

{% match app.dl_module %}
  {%- when Some with (module) -%}
This app is translated via the [GNOME translation system](https://l10n.gnome.org) called *Damned Lies*.

<ul class="links">
  {%- let url = format!("https://l10n.gnome.org/module/{}/", module) -%}
  {%- call macro::link("translation", url, "Translation on Damned Lies"|t, "The app’s translation status on GNOME’s localization system"|t) -%}
  {%- call macro::link("docs", "https://wiki.gnome.org/TranslationProject", "Translation Project"|t, "More about the translation project in GNOME"|t) -%}
  {%- call macro::link("chat", "https://matrix.to/#/#i18n:gnome.org", "#i18n:gnome.org"|t, "Get help in the matrix channel of the translation project"|t) -%}
</ul>
  {% when None -%}
    {%- match app.url_translate -%}
      {%- when Some with (url) -%}
        {%- if url.contains("https://hosted.weblate.org") -%}
The translation of this project is managed via Weblate hosting. This platform is not part of the GNOME project.
<ul class="links">
  {%- call macro::link("translation", url, "Translate on Weblate"|t, "Project translation on Weblate hosting"|t) -%}
</ul>
        {%- else -%}
<ul class="links">
  {% call macro::link("translation", url, "Translate this app"|t, "Learn more about how to translate this app"|t) %}
</ul>
        {% endif %}
      {%- when None -%}
It looks like this app does not provide a way to be translated, yet. Feel free to file an issue against the project to ask for a way to translate this app.
  {% endmatch %}
{% endmatch %}

## Trying the latest version

{% if !app.nightly_id.is_some() && !app.on_flathub %}
For technical reasons, the latest version of this app cannot be easily tested. This is the case for very few apps that need tight integration with the system.
{% else %}
When reporting an issue or suggesting a feature, it can be helpful to base the report on the latest version of the app. To this end, you can check the installation methods below.
{% endif %}

{# Some apps like Software or Settings are not on Flathub #}
{% if app.on_flathub %}
### Installation from Flathub

<p>{{ "If you found an issue or want to propose a change you should try to make sure you have tried out the latest version of {0}. You can check the current version in the app’s “About” section."|t|f([app.name.as_str()])|markdown }}</p>

{% match app|latest_release(false) %}
  {% when Some with (release) -%}
    {{ "The latest released version on Flathub is version {0}."|t|f([release.version.as_str()])|markdown }}
  {% when None %}
{% endmatch %}

<ul class="links">
  {% let url = "https://flathub.org/apps/details/{}"|format(app.id) %}
  {% call macro::link("installation", url, "Install from Flathub"|t, "Install latest version of the app from Flathub"|t) %}
</ul>
{% endif %}

{% match app.nightly_id %}
  {% when Some with (nightly_id) -%}
### Installing a nightly build

This app allows to install a version of its current development status. Such installations were traditionally generated each night, giving them the name “nightly builds.” Nowadays, they are usually regenerated whenever the main development version changes.

<div class="warning">

**Warning:** Nightly builds of apps should only be used for testing and not for actual tasks. You can identify nightly builds of the app on your system via the stripes in the app’s icon and the stripes in the app’s headerbar. They usually use a different configuration than other installations of this app. However, there is no guarantee that the installation of these apps will work as intended. Never work on data with nightly builds of apps without having backups available.

</div>

<ul class="links">
  {% let url = "https://nightly.gnome.org/repo/appstream/{}.flatpakref"|format(nightly_id) %}
  {% call macro::link("installation", url, "Install Nightly"|t, "Install latest build of the app"|t) %}
  {% call macro::link("docs", "http://nightly.gnome.org", "GNOME Nightly"|t, "Learn more about GNOME nightly apps."|t) %}
</ul>

The nightly version can also be installed via [Console](https://apps.gnome.org/app/org.gnome.Console/) if the nightly repository is already [configured](http://nightly.gnome.org).

```
$ flatpak install gnome-nightly {{ nightly_id }}
$ flatpak run {{ nightly_id }}
```
  {% when None -%}
{% endmatch %}

### Quality testing

Catching issues and regressions before they land in a stable release is very valuable for a project. You can help with this by trying out an upcoming release.

{%- if app.gnome == "core" %}
 {{ "This is especially helpful during the [feature freeze period](https://wiki.gnome.org/ReleasePlanning/Freezes) which is dedicated to finding and fixing bugs."|t|markdown }}
{%- endif %}
{%- if app.nightly_id.is_some() %}
 You can do this by trying out the nightly build of the app.
{%- else %}
 {{ "You can do this by [manually building the app](#getting-the-app-to-build) and trying it out."|t|markdown }}
{%- endif -%}
{%- if app.gnome == "core" %}
 {{ "As a Core app, it is also pre-installed on [GNOME OS Nightly](https://os.gnome.org/) which you can install as a virtual machine."|t|markdown }}
{%- endif %}
</p>

## Chasing issues

For many apps, the issue tracker is a central place for coordination of app development. It is not only used to track all existing problems but also to plan new features and other miscellaneous tasks.

{% match app.url_bugtracker %}
  {%- when Some with (url) -%}
    <ul class="links">
      {% call macro::link("issues", url, "Issue Tracker"|t, "View existing issues and feature requests or add new ones"|t) %}
    </ul>
  {%- when None -%}
This app does not currently provide information on where to inspect issues.
{% endmatch %}

### Reporting found issues

Reporting issues you found when using the app can be of high value for app maintainers. However, processing issue reports can also take considerable amount of time. You can take on some of the work by trying the following steps:

- Search the issue tracker to find out if the issue has already been reported.
- Check if the issue also exists in the [latest version of the app](#trying-the-latest-version).
- If applicable, try to find out under which exact circumstances the problem occurs.
- Report all potentially relevant information, like the version of the app that expose the behavior.

### Diagnosing listed issues

Tracking down the exact cause of an issue can be a very important task. Especially for issues tagged with labels like “Needs Diagnosis” or “Needs Information.” If you can reproduce, that is, trigger the reported problem yourself, you can try to dig deeper. This can go from finding out under which exact conditions the issue appears, over running the app with [debug output enabled](https://developer-old.gnome.org/glib/unstable/glib-running.html), to adding debug outputs to the code that narrow down the problem.

### Suggesting features

Most apps handle feature requests via the issue tracker as well. Note that implementing features and maintaining them as part of the codebase in the future can require a significant amount of additional work. Therefore, you should consider whether a new feature could even be relevant enough to be part of this app.

When suggesting a new feature, it is helpful to focus on the question of what practical problem the feature should solve. Not on how the app should solve the problem or how exactly an implementation would look like. Preferably, you can also give a practical example where you needed the feature.

### Finding a task

The issue tracker can also help you find open tasks you could tackle. Before implementing a new feature, it is usually a good idea to check in with the maintainers if merge requests for this feature would be accepted.

{% match app.url_bugtracker %}
  {%- when Some with (url) -%}
    {%- if url.contains("gitlab.gnome.org/GNOME/") -%}
This app also has tasks which are labeled with the "Newcomer" tag. Those tasks usually require less prior knowledge or are less complex.

<ul class="links">
  {% call macro::link("issues", "{}?label_name%5B%5D=4.%20Newcomers"|format(url), "“Newcomer” issues"|t, "View issues labeled “Newcomer” in the issue tracker"|t) %}
</ul>
    {%- endif -%}
  {%- when None -%}
{% endmatch %}

## Working on the code

Whether you want to fix a typo, change the user interface, or work on the code of the app, the following information can help you get started. The first step will be to get the app built on your computer, allowing you to play around with your changes. Afterward, you can check the provided documentation on how to get respective tasks done. The subsequent section will show you how to submit your changes.

If you are planning a somewhat larger change or addition to the app, it is often a good idea to check in with the maintainers of the app to find out if that change would be accepted. For this, you should first convince yourself that you can tackle the task at hand by doing the very first implementation steps and then check in with the maintainers via the issue tracker. This is also a good way to check if the project has an active maintainer who can accept your contribution. Despite our best efforts, not all projects are always continuously maintained.

Note that while many community members are happy to help you out when you are stuck, maintainers oftentimes might not have the resources to guide you through a contribution.

{% let create_app_url = "{0}/team/programming/#creating-a-new-app"|f([site.lang_base_url.as_str()]) %}
<p>{{ "If you are overwhelmed by working on an existing GNOME app, you can also look into [building your own app]({0}) for learning purposes first."|t|f([create_app_url])|markdown }}</p>

<ul class="links">
  {% call macro::link("gitlab", app.repo_url, "Code Repository"|t, "The project’s home, keeping the source code and more information"|t) %}
  {%- match app.url_contact -%}
    {% when Some with (url) %}
      {%- if url.contains("https://matrix.to/") -%}
        {% let room = url.replace("https://matrix.to/#/", "") %}
        {% call macro::link("chat", url, room, "The app’s Matrix chat for project specific questions"|t) %}
      {%- endif -%}
    {%- when None -%}
  {%- endmatch -%}
  {% call macro::link("docs", "https://developer.gnome.org/", "GNOME Development Center"|t, "Overview of many important development resources for GNOME"|t) %}
</ul>

### Getting the app to build

{% if app.id.to_string() == "org.gnome.Extensions" %}

There is no information available on how to build this app.

{% else %}

When making changes to an app you will usually want to build and test the app with its changes on your computer. For most apps this process can be simplified a lot by using the Builder app.

<ul class="links">
  {% call macro::link("installation", "https://flathub.org/apps/details/org.gnome.Builder", "Install Builder"|t, "Install the Builder app from Flathub"|t) %}
</ul>

After starting Builder, you can choose to *clone* a project. This will download the app to your computer.

<div class="mock-window fade-top">
  <div class="mock-headerbar" style="margin-top: 70px;">
    <div class="mock-button-group">
    <div class="mock-button"></div>
    <div class="mock-button"></div>
    <div class="mock-button highlight">{{ "Clone Repository…"|t }}</div>
      </div>
  </div>
</div>

You have to enter the repository location. The correct URL can be copied from below.

<div class="mock-window fade-top">
  <div style="padding-top: 50px; text-align: center;">
    <!-- Extra div to limit double click selection to URL -->
    <div>
      <!-- TODO: Use correct git url from repo -->
      <div class="mock-entry-row highlight">
        <div class="label">{{ "Repository URL"|t }}</div>
        <input readonly value="{{ app.repo_url }}.git"/>
      </div>
    </div>
    </div>
  <div style="padding: 50px 20px 20px 20px; text-align: end;">
    <div class="mock-button highlight">{{ "Clone Repository"|t }}</div>
  </div>
</div>

Depending on your internet connection and the size of the project, cloning the project might take a while. After Builder has completed this step, the project window should open. You might have to confirm the installation of some required dependencies. To try if you can successfully build and start the app, you can now press the “Run Project” button.

<div class="mock-window fade-bottom" style="padding-bottom: 70px;">
  <div class="mock-headerbar">
    <div class="mock-entry mock-label">{{ app.name }}</div>
    <div class="mock-button highlight" style="margin-inline-start: 9px;">{% include "icons/play-symbolic.svg" %}</div>
  </div>
</div>

If everything goes right, the app should be built. After the build has been completed, the built app should start automatically.

{% set gtk_version = app.gtk_version %}
{% for plang in app.programming_languages %}
  {% let prelude = "This app is built with the *{0}* programming language."|t|f([plang])|markdown %}
  {% let lang_id =  plang.to_lowercase() %}
  {%- if lang_id == "c" -%}
### C language
{{ prelude }}

{% include "_programming-language/c.md" %}
  {%- else if lang_id == "javascript" -%}
### JavaScript language
{{ prelude }}

{% include "_programming-language/javascript.md" %}
  {%- else if lang_id == "python" -%}
### Python language
{{ prelude }}

{% include "_programming-language/python.md" %}
  {%- else if lang_id == "rust" -%}
### Rust language
{{ prelude }}

{% include "_programming-language/rust.md" %}
  {%- else if lang_id == "vala" -%}
### Vala language
{{ prelude }}

{% include "_programming-language/vala.md" %}
  {%- else -%}
      <!-- Undocumented programming language '{{ plang }}' -->
  {% endif %}
{% endfor %}

{# Ending special casing for org.gnome.Extensions #}
{% endif %}

### Useful apps

<div>
  <ul class="app-grid">
    {% call macro::app("org.gnome.Builder") %}
    {% call macro::app("org.gnome.Console") %}
    {% call macro::app_manual(
        "gitg"|t,
        "https://flathub.org/apps/details/org.gnome.gitg",
        "https://dl.flathub.org/repo/appstream/x86_64/icons/128x128/org.gnome.gitg.png"
    ) %}
  </ul>
</div>

## Submitting your work

After making changes to a project, you can submit them for review. Our goal is to create what’s called a *merge request* or *pull request*. That means suggesting a change to a project’s code and data.

{% let git_forge %}
{% let git_forge_keys %}
{% let git_forge_fork %}
{% let git_forge_register %}

{% let git_forget_is_github = app.repo_url.contains("github.com/") %}

{% if git_forget_is_github %}
### Setting up GitHub
  {% let git_forge = "GitHub"|t %}
  {% let git_forge_fork = app.repo_url.clone() + "/fork" %}
  {% let git_forge_keys = "https://github.com/settings/keys".to_string() %}
  {% let git_forge_register = "https://github.com/signup".to_string() %}
{% else %}
  {% let git_forge_url %}
  {% if app.repo_url.contains("gitlab.gnome.org/") %}
    {% let git_forge_url = "https://gitlab.gnome.org/" %}
    {% let git_forge = "GNOME GitLab"|t %}
  {% else %}
    {% let git_forge_url = "https://gitlab.com/" %}
    {% let git_forge = "GitLab"|t %}
  {% endif %}
### Setting up GitLab
  {% let git_forge_fork = app.repo_url.clone() + "/-/forks/new" %}
  {% let git_forge_keys = git_forge_url.to_string() + "-/profile/keys" %}
  {% let git_forge_register = git_forge_url.to_string() + "users/sign_up" %}
{% endif %}

<p>{{ "We begin with some preparations on {0}, where the project is hosted. This process might look quite involved if you are going through it for the first time, but you will become much more accustomed to it over time."|tc("The {0} will be replaced with 'GitLab' or 'GitHub'")|f([git_forge.as_str()]) }}</p>

<ol class="long">
  <li>
    {{ "The first step is to create a [new {0} account]({1}) if you don’t own one already. You just have to use the linked webform for that."|tc("The {0} will be replaced with the correct URL and {1} with 'GitLab' or 'GitHub'")|f([git_forge.as_str(), git_forge_register.as_str()])|markdown }}
  </li>
  <li>
    {{ "Next, we want that git on your computer can also use your {0} account. For this, you need an SSH key. If you don't have an SSH key yet you can [generate a new one](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair) using [Console](https://apps.gnome.org/Console/). Alternatively, you can generate one using the [Passwords and Keys](https://flathub.org/de/apps/org.gnome.seahorse.Application) app."|tc("The {0} will be replaced with 'GitLab' or 'GitHub'")|f([git_forge.as_str()])|markdown }}
  </li>
  <li>
    {{ "You now have to add your public SSH key to the [“SSH Keys” setting]({0}) on {1}. Public SSH keys are stored in the `.ssh` folder in your home directory as files with the ending `.pub`. You can print all your public SSH keys via the command `cat ~/.ssh/*.pub`. You can also view the public SSH keys using the *Passwords and Keys* app."|tc("The {0} will be replaced with the correct URL and {1} with 'GitLab' or 'GitHub'")|f([git_forge_keys.as_str(), git_forge.as_str()])|markdown }}
  </li>
</ol>

### Creating your own project copy

<p>{{ "To suggest changes to a project, you first have to create your own copy of the project on {0}. Creating this copy is also referred to as *forking*. You will need your own copy – or *fork* – of the project to upload your changes to this copy."|tc("The {0} will be replaced with 'GitLab' or 'GitHub'")|f([git_forge.as_str()])|markdown }}</p>

<ol class="long">
  <li>
    {{ "First, you can fork *{2}* via the [{1} page]({0}). You only need to do this once for every project you work on."|tc("The {0} will be replaced with the correct URL and {1} with 'GitLab' or 'GitHub, and {2} with the app name'")|f([git_forge_fork.as_str(), git_forge.as_str(), app.name.as_str()])|markdown }}
  </li>
  <li>
{{ "We now have to find out the SSH URL for your fork. You can find it on the page you are redirected to after creating the fork. The page of the fork will also be listed on your profile."|t }}
{%- if git_forget_is_github -%}
{{ " " }}{{ "The URL is available under the button “Code” below “SSH.”"|t }}
{%- else -%}
{{ " " }}{{ "The URL is available under the button “Clone” below “Clone with SSH.”"|t }}
{%- endif -%}
  </li>
  <li>
{{ "Now, we have to let git in your local project know of your personal copy on {0} via this URL. For this we need to go back to the command line. First, you have to change ([`cd`](https://www.gnu.org/software/bash/manual/bash.html#index-cd)) into the project you have been working on. Afterwards you can use the command"|tc("The {0} will be replaced with 'GitLab' or 'GitHub'")|f([git_forge.as_str()])|markdown }}

```
$ git remote add my-copy <ssh-fork-url>
```

where `<ssh-fork-url>` has to be replaced with the SSH URL you looked up in the previous step.

  </li>
</ol>

### Creating a merge request

We now arrive at what are more regular tasks when working with git.

<ol class="long">
  <li>

First, we want to switch to what’s called a different *branch*. Branches are variants of the original code where you can, for example, develop a feature, until it is ready to go into the actually used code. To do this you can call

```
$ git checkout -b my-changes
```

where `my-changes` is a short description of what you are working on, without spaces.

  </li>
  <li>

Next, we want to save the changes you made with git. That’s usually referred to as creating a *commit*. You can do this with the command

```
$ git commit -am "Commit Message"
```

The commit messages should describe your changes. You can learn more about [choosing a fitting commit message](https://handbook.gnome.org/development/commit-messages.html).

  </li>
  <li>

Now, we have to get your saved changes onto GitLab. Luckily, we have already done all the setup with using `git remote add` in the previous section. All that’s left to do is now to call

```
$ git push my-copy
```

  </li>
  <li>

We are finally at the last step. If you check the output in Console after our previous `git push` command, you will probably already see a website link showing you the way to create a *pull* or *merge request*. You can open it by pressing the *Ctrl* key while clicking on it. All that is left is to follow the website to finalize submitting your changes.

  </li>
</ol>


</div></section>
