{% import "_macros.html" as macro %}

The GNOME design team is responsible for GNOME user experience and visual design. This includes:

* the design of the GNOME desktop, core apps, and app toolkit components
* icon and wallpaper design
* interface styling
* maintaining GNOME's design guidelines
* usability testing and research

Help is needed in all of these areas, and contributions are welcome.

## Get involved

New contributors should join the GNOME design chat channel and see the GNOME design team GitLab space.

<ul class="links">
  {% call macro::link("gitlab", "https://gitlab.gnome.org/Teams/Design", "Design Team on GitLab"|t, "Design team issue tracking and work archive"|t) %}
  {% call macro::link("chat", "https://matrix.to/#/#design:gnome.org", "#design:gnome.org", "The main channel for GNOME design discussions"|t) %}
</ul>

## Where to contribute

GNOME design work happens in a variety of places, depending on the type of contribution:

* To contribute directly to the design of an app or module, use its project in GitLab or its Matrix chat channel.
* Many contributions happen through discussion with other designers and developers, often in chat on Matrix.
* Contributions to icons and wallpapers happen in dedicated GitLab projects, whose details can be found in [GNOME design team GitLab space](https://gitlab.gnome.org/Teams/Design).
* Ideas for design changes can be suggested in the [design team's whiteboard project](https://gitlab.gnome.org/Teams/Design/whiteboards).
* New user experience designs are stored and tracked in the team's public mockup repositories, which can also be found in GitLab.

## Getting up to speed

Everyone who works on GNOME user experience and design should be familiar with:

* GNOME's design guidelines, called the [GNOME Human Interface Guidelines](https://developer.gnome.org/hig/).
* Existing GNOME design work, including the current state of GNOME itself, as well as our latest design work.
* The posts in the design team's [recommended reading list](https://hedgedoc.gnome.org/design-reading?view#), particularly those by Havoc Pennington (for some insight into the history behind GNOME design work).

Participating in GNOME design does not entail any other requirements for experience or training. However, the role that you are able to play in GNOME design will depend on your existing level of expertise: experienced designers may be able to take on design initiatives relatively quickly, but if you are new to design work, you may need to gain experience on smaller tasks to begin with.

## Joining the team

Becoming a core member of the design team requires a time commitment, since you must work closely with the rest of the team and build up a detailed knowledge of our design language and practices. If this is something that you'd like to do, making contact with an existing team member for guidance is a good first step.

## Other ways to help

There are many ways to contribute to GNOME user experience. If you are new and want to help out, here are some great ways to get started:

* Try contributing user experience changes directly to GNOME modules. This can include even simple changes such as icon or text label changes.
* Report user experience problems in GNOME's issue trackers.
* Help out in the issue trackers of GNOME modules, to review reports and help to find solutions to design problems.
 
These activities will often require you to work with the design team to come up with solutions to design problems.
