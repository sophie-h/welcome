{% import "_macros.html" as macro %}

## Contribute to existing apps

Below, you can find instructions on how to build and work on specific apps. If you are not sure where to contribute, consider choosing an app that you use yourself or select a programming language with which you are already familiar. Try to solve a problem or implement a feature that is important to you or where you feel an inner motivation.

{% include "_app-search.html" %}

## Creating a new app

Creating your own app will not only allow you to implement your own ideas at will. It is also an opportunity to understand how all the pieces in the GNOME ecosystem fit together.

It is possible to write apps for GNOME in several different programming languages. You can check our list of the [most frequently used languages](https://developer.gnome.org/documentation/introduction/languages.html).

You can publish your app on [Flathub](https://docs.flathub.org/docs/for-app-authors/submission/) to make it easily available to many people. If your app is polished and ready for everyday usage, you can consider submitting it to [GNOME Circle](https://circle.gnome.org). The Circle membership makes your project eligible for support through the GNOME Foundation.

<ul class="links">
  {% call macro::link("docs", "https://developer.gnome.org/documentation/introduction/languages.html", "Programming Languages"|t, "Explore which languages can be used to write GNOME apps"|t) %}
  {% call macro::link("docs", "https://docs.flathub.org/docs/for-app-authors/submission/", "Publish on Flathub"|t, "Learn how you can publish your app on Flathub"|t) %}
  {% call macro::link("gitlab", "https://gitlab.gnome.org/", "GNOME GitLab"|t, "Use GNOME’s infrastructure to host your project"|t) %}
  {% call macro::link("chat", "https://matrix.to/#/#newcomers:gnome.org", "#newcomers:gnome.org", "The “GNOME Newcomers” chat can help you get started"|t) %}
</ul>

### Programming languages

Below, you can find an overview of the most popular languages for writing GNOME apps.

<div class="tabbed">
  <input id="C" type="radio" name="tabs" checked="checked" />
  <input id="JavaScript" type="radio" name="tabs" />
  <input id="Python" type="radio" name="tabs" />
  <input id="Rust" type="radio" name="tabs" />
  <input id="Vala" type="radio" name="tabs" />

  <nav>
    <label for="C">C</label>
    <label for="JavaScript">JavaScript</label>
    <label for="Python">Python</label>
    <label for="Rust">Rust</label>
    <label for="Vala">Vala</label>
  </nav>

{% set gtk_version = Some(4) %}
  <figure>
<div id="content-C">

{% include "_programming-language/c.md" %}

</div>
<div id="content-JavaScript">

{% include "_programming-language/javascript.md" %}

</div>
<div id="content-Python">

{% include "_programming-language/python.md" %}

</div>
<div id="content-Rust">

{% include "_programming-language/rust.md" %}

</div>
<div id="content-Vala">

{% include "_programming-language/vala.md" %}

</div>
   </figure>
</div>

### Useful apps

The following apps are worth trying out to get started with development within GNOME. For more apps you can check the [Development Tools section](https://apps.gnome.org/#development) on *Apps for GNOME*.

<div>
  <ul class="app-grid">
    {% call macro::app_manual(
        "Adwaita Demo"|t,
        "https://nightly.gnome.org/repo/appstream/org.gnome.Adwaita1.Demo.flatpakref",
        "https://gitlab.gnome.org/GNOME/libadwaita/-/raw/main/demo/data/org.gnome.Adwaita1.Demo.svg?inline=false"
    ) %}
    {% call macro::app("org.gnome.Builder") %}
    {% call macro::app("re.sonny.Workbench") %}
  </ul>
</div>
