For getting started with the [Vala language](https://vala.dev/), we recommend the [Vala Tutorial](https://docs.vala.dev/tutorials/programming-language/main.html) or the [Beginners Tutorial](https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html), which also covers Vala. You can find other important resources below.

<ul class="links">
  {% call macro::link("docs", "https://docs.vala.dev", "Official Vala Documentation"|t, "All information about the Vala programming language for users, its syntax, tooling, and many code examples"|t) %}
  {% call macro::link("api-docs", "https://valadoc.org", "Vala Bindings API References"|t, "How to use GTK, the underlying library for user interfaces, and other libraries"|t) %}
  {% call macro::link("repo", "https://gitlab.gnome.org/GNOME/vala", "Development Platform"|t, "Main development repository of the Vala language and compiler, issue tracker and contribution platform"|t) %}
  {% call macro::link("chat", "https://matrix.to/#/#vala:gnome.org", "#vala:gnome.org", "Matrix channel for all Vala related questions"|t) %}
</ul>
