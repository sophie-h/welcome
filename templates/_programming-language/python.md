If you are familiar with the basics of the [Python language](https://www.python.org/), we recommend the [Beginners Tutorial](https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html) which also covers the Python language. You can find other important resources below.

<ul class="links">
  {%- if gtk_version == Some(3) -%}
    {% call macro::link("docs", "http://lazka.github.io/pgi-docs/#Gtk-3.0", "GTK&nbsp;3 docs"|t, "GTK is the underlying library for the user interface"|t) %}
    {% call macro::link("docs", "http://lazka.github.io/pgi-docs/#Handy-1", "Libhandy docs"|t, "Building blocks for modern adaptive GNOME apps"|t) %}
  {%-else -%}
    {% call macro::link("docs", "http://lazka.github.io/pgi-docs/#Gtk-4.0", "GTK&nbsp;4 docs"|t, "GTK is the underlying library for the user interface"|t) %}
    {% call macro::link("docs", "http://lazka.github.io/pgi-docs/#Adw-1", "Libadwaita docs"|t, "Building blocks for modern GNOME apps"|t) %}
  {% endif %}
  {% call macro::link("chat", "https://matrix.to/#/#python:gnome.org", "#python:gnome.org", "Matrix channel for all questions related to Python in GNOME"|t) %}
</ul>
