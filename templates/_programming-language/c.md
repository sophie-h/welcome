To get started with developing GNOME apps with C we recommend the [Beginners Tutorial](https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html) that also covers the C language. You can find other important resources below.

<ul class="links">
  {%- if gtk_version == Some(3) -%}
    {% call macro::link("docs", "https://docs.gtk.org/gtk3/", "GTK&nbsp;3 Docs"|t, "GTK is the underlying library for the user interface"|t) %}
    {% call macro::link("docs", "https://gnome.pages.gitlab.gnome.org/libhandy/doc/1-latest/", "Libhandy Docs"|t, "Building blocks for modern adaptive GNOME apps"|t) %}
  {%- else -%}
    {% call macro::link("docs", "https://docs.gtk.org/gtk4/", "GTK&nbsp;4 Docs"|t, "GTK is the underlying library for the user interface"|t) %}
    {% call macro::link("docs", "https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/", "Libadwaita Docs"|t, "Building blocks for modern GNOME apps"|t) %}
  {% endif %}

  {% call macro::link("docs", "https://www.gtk.org/docs/apis/", "More API References"|t, "More documentation for GObject libraries"|t) %}
</ul>
